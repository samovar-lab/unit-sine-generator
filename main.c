#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/***
 * Parameters
 ***/

const char* PARAMETER_HZ_KEY = "HZ";
const char* PARAMETER_SAMPLING_RATE_KEY = "SAMPLING_RATE";
const char* PARAMETER_DURATION_KEY = "DURATION";

typedef struct {
    float hz;
    int sampling_rate;
    float duration; 
} Parameters;

Parameters*
parameters_create(void) {
    Parameters* self = (Parameters*) calloc(1, sizeof(Parameters));
    self->hz = 100;
    self->sampling_rate = 44100;
    self->duration = 10;
    return self;
}

/***
 * Generator
 ***/

typedef struct {
    int samples;
    float* buffer;
} Generator;

Generator*
generator_create(
    float hz,
    int sampling_rate,
    float duration
) {
    Generator* self = (Generator*) calloc(1, sizeof(Generator));

    // Calculate amount of samples
    const int samples = (int)(((double)sampling_rate) * ((double)duration));
    self->samples = samples;

    // Allocate a clear buffer
    float* buffer = (float*) calloc(
        self->samples, 
        sizeof(float)
    );
    self->buffer = buffer;

    // Calculate samples
    const double PI = atan(1)*4;
    const double PI_2_TIMES = PI * 2;
    const int phase_resolution = 1000000;
    const int phase_resolution_d = (double)phase_resolution;

    const int phase_delta_per_sample = (int)(((double)phase_resolution) / (((double)sampling_rate) * ((double)hz)));
    
    int phase = 0;
    for(int i=0; i < samples; i++) {
        double phase_normalized = PI_2_TIMES * (((double)phase) / phase_resolution_d);
        buffer[i] = sin(phase_normalized);
        phase += phase_delta_per_sample;
        phase %= phase_resolution;
    }


    return self;
}

/***
 * Entry point
 ***/ 

int
main (void)
{
    Generator* g = generator_create(100, 44100, 10.5);
    return 0;
}