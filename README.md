# Unit : Sine Generator

## Introduction

This unit takes some parameters and generates a WAV file on stdout which will contain a single channel with a sine wave

## Example call

```bash
set UNIT_SG_HZ="100"
set UNIT_SG_SAMPLING_RATE="44100"
set UNIT_SG_DURATION_IN_SEC="10.5"
./sine_generator UNIT_SG
```